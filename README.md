# CSV to JSON Microservice for Zeit Now 2

> Send a HTTP POST request with CSV data and have it returned as a JSON object to use in your web projects.

[![Deploy to now](https://deploy.now.sh/static/button.svg)](https://deploy.now.sh/?repo=https://github.com/jake-101/csv-to-json-microservice)
[![Run in Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=CSV%20to%20JSON%20Microservice&uri=https%3A%2F%2Fcsv-to-json.now.sh%2Finsomnia.json)

MIT License
